/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.projectdcoffee.ui;

import com.mycompany.databaseproject.service.StockService;
import com.mycompany.projectdcoffee.model.Stock;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author test1
 */
public class StockPenel extends javax.swing.JPanel {

    private StockService stockService;
    private TableModel model;
    private final List<Stock> stockList;

    /**
     * Creates new form StockPenel
     */
    public StockPenel() {

        initComponents();
        
        
        
        
        stockService = new StockService();
        stockList = stockService.getStocks();
        model = new AbstractTableModel() {
            String[] colNames = {"ID","Name","Quantity","Minimum","Unit","Status"};
            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }
            
            @Override
            public int getRowCount() {
                return stockList.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Stock artist = stockList.get(rowIndex);
                switch(columnIndex){
                    case 0 :
                        return artist.getId();
                    case 1 :
                        return artist.getName();
                    case 2 :
                        return artist.getQty();
                    case 3 :
                        return artist.getMinimum();
                    case 4 :
                        return artist.getUnit();   
                    case 5 :
                        return artist.getStatus();
                    default:
                        return "" ;
                }
            }
        };
        tblStock.setModel(model);
        
        
                     
        
        
        
        
        
        
//        userService = new UserService();
//        tblUser.setRowHeight(100);
//        tblUser.setModel(new AbstractTableModel() {
//            String[] columnNames = {"ID", "Login", "Name", "Password", "Gender", "Role"};
//
//            @Override
//            public String getColumnName(int column) {
//                return columnNames[column];
//            }
//
//            @Override
//            public int getRowCount() {
//                return list.size();
//            }
//
//            @Override
//            public int getColumnCount() {
//                return 6;
//            }
//
//            @Override
//            public Class<?> getColumnClass(int columnIndex) {
//                switch (columnIndex) {
//                    case 0:
//                        return ImageIcon.class;
//                    default:
//                        return String.class;
//                }
//            }
//
//            @Override
//            public Object getValueAt(int rowIndex, int columnIndex) {
//                User user = list.get(rowIndex);
//                switch (columnIndex) {
//                    case 0:
//                        ImageIcon icon = new ImageIcon("./user" + user.getId() + ".png");
//                        Image image = icon.getImage();
//                        int width = image.getWidth(null);
//                        int height = image.getHeight(null);
//                        Image newImage = image.getScaledInstance((int) (100 * (float) width / height), 100, Image.SCALE_SMOOTH);
//                        icon.setImage(newImage);
//                        return icon;
//                    case 1:
//                        return user.getId();
//                    case 2:
//                        return user.getLogin();
//                    case 3:
//                        return user.getName();
//                    case 4:
//                        return user.getPassword();
//                    case 5:
//                        return user.getGender();
//                    case 6:
//                        return user.getRole();
//                    default:
//                        return "Unknown";
//                }
//            }
//        });///////////////////////////////////////////////////

//        tblUser.setRowHeight(100);
//        Stock newStock = new Stock(1, "Med Coffee", 20, 10, "Kg", "Full");
//        stockservice.addStock(newStock);
//        tableStock.setModel(new AbstractTableModel() {
//            @Override
//            public int getRowCount() {
//                return list.size();
//            }
//
//            @Override
//            public int getColumnCount() {
//                return 6;
//            }
//            
//            @Override
//            public Object getValueAt(int rowIndex, int columnIndex) {
//                Stock stock = list.get(rowIndex);
//                if(columnIndex==0){
//                    return stock.getId();
//                }else if(columnIndex==1){
//                    return stock.getName();   
//                }else if(columnIndex==2){
//                    return stock.getQty();   
//                }else if(columnIndex==3){
//                    return stock.getMinimum();   
//                }else if(columnIndex==4){
//                    return stock.getUnit();   
//                }else if(columnIndex==5){
//                    return stock.getStatus();   
//                }
//                return "";
//               
//            }
//            String[] columnNames = {"ID", "Login", "Name", "Password", "Gender", "Role"};
//            @Override
//            public String getColumnName(int column) {
//                switch (column) {
//                    case 0:
//                        return "ID";
//                    case 1:
//                        return "NAME";
//                    case 2:
//                        return "QTY"; 
//                    case 3:
//                        return "MINIMUM";
//                    case 4:
//                        return "UNIT";
//                    case 5:
//                        return "STATUS";
//                    default:
//                        break;
//                }
//                return "";
//            }
//            
//        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblStock = new javax.swing.JTable();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        JStable = new javax.swing.JScrollPane();

        setBackground(new java.awt.Color(102, 0, 0));

        tblStock.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title5", "Title6", "Title7"
            }
        ));
        jScrollPane1.setViewportView(tblStock);

        jTextField1.setFont(new java.awt.Font("Segoe UI", 0, 48)); // NOI18N
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField1.setText("Stock Management");
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jButton1.setText("History");

        jButton3.setText("Print");

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 862, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 475, Short.MAX_VALUE)
                        .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(JStable))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JStable, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
       JStable.setViewportView(new StockEditQty());
    }//GEN-LAST:event_btnEditActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane JStable;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTable tblStock;
    // End of variables declaration//GEN-END:variables


}
