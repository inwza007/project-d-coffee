/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.mycompany.projectdcoffee.ui;

//import com.mycompany.databaseproject.component.BuyProductable;

//import com.mycompany.databaseproject.component.MaterialListPanel;
//import com.mycompany.databaseproject.service.MaterialService;
import com.mycompany.databaseproject.component.BuyMaterialTable;
import com.mycompany.databaseproject.component.MaterialListPanel;
import com.mycompany.projectdcoffee.model.Material;
import com.mycompany.projectdcoffee.model.MaterialReciept;
import com.mycompany.projectdcoffee.model.MaterialRecieptDetail;
//import java.awt.Font;
//import java.awt.Image;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
//import javax.swing.ImageIcon;
//import javax.swing.table.AbstractTableModel;

//import com.mycompany.databaseproject.component.ProductListPanel;
//import com.mycompany.databaseproject.model.Product;
//import com.mycompany.databaseproject.model.Reciept;
//import com.mycompany.databaseproject.model.RecieptDetail;
//import com.mycompany.databaseproject.service.ProductService;
//import com.mycompany.databaseproject.service.RecieptService;
//import com.mycompany.databaseproject.service.UserService;
//import java.awt.Font;
//import java.awt.Image;
//import java.awt.event.MouseAdapter;
//import java.awt.event.MouseEvent;
//import java.util.ArrayList;
//import javax.swing.ImageIcon;
//import javax.swing.table.AbstractTableModel;

/**
 *
 * @author gortorr
 */
public class ShopMaterial extends javax.swing.JFrame implements BuyMaterialTable{

    private ArrayList<Material> materials;
    MaterialReciept materialReciept ;
    private final MaterialListPanel materialListPanel;


    /**
     * Creates new form StockImport
     */
    
    public ShopMaterial() {
        initComponents();
        materialListPanel = new MaterialListPanel();
        materialListPanel.addOnBuyProduct(this);
        ProductList.setViewportView(materialListPanel);
        
        materialReciept = new MaterialReciept();
        tblShopMaterialReciept.setModel(new AbstractTableModel() {
            String[] headers = {"Name", "Price", "Qty", "Total","Unit"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return materialReciept.getMaterialRecieptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<MaterialRecieptDetail> recieptDetails = materialReciept.getMaterialRecieptDetails();
                MaterialRecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getId();
                    case 1:
                        return recieptDetail.getMaterialId();
                    case 2:
                        return recieptDetail.getQty();
                    case 3:
                        return recieptDetail.getTotalPrice();
                    case 4:
                        return recieptDetail.getUnit();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<MaterialRecieptDetail> recieptDetails = materialReciept.getMaterialRecieptDetails();
                MaterialRecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                if (columnIndex==2) {
                    int qty = Integer.parseInt((String) aValue);
                    if(qty<1) return ;
                    recieptDetail.setQty(qty);
                    materialReciept.calculateTotal();
                    refreshReciept();
                }
         
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch(columnIndex) {
                    case 2:
                        return true;
                    default :
                        return false;
                }
            }

        });
        
        
//        materialReciept = new MaterialReciept();
//        lblUserName.setText(UserService.getCurrentUser().getName());
//        reciept.setUser(UserService.getCurrentUser());
//        pnlProductList.setModel(new AbstractTableModel() {
//            String[] headers = {"Name", "Price", "Qty", "Total"};
//
//            @Override
//            public String getColumnName(int column) {
//                return headers[column];
//            }
//
//            @Override
//            public int getRowCount() {
//                return reciept.getRecieptDetails().size();
//            }
//
//            @Override
//            public int getColumnCount() {
//                return 4;
//            }
//
//            @Override
//            public Object getValueAt(int rowIndex, int columnIndex) {
//                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
//                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
//                switch (columnIndex) {
//                    case 0:
//                        return recieptDetail.getProductName();
//                    case 1:
//                        return recieptDetail.getProductPrice();
//                    case 2:
//                        return recieptDetail.getQty();
//                    case 3:
//                        return recieptDetail.getTotalPrice();
//                    default:
//                        return "";
//                }
//            }
//
//            @Override
//            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
//                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
//                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
//                if (columnIndex==2) {
//                    int qty = Integer.parseInt((String) aValue);
//                    if(qty<1) return ;
//                    recieptDetail.setQty(qty);
//                    reciept.calculateTotal();
//                    refreshReciept();
//                }
//         
//            }
//
//            @Override
//            public boolean isCellEditable(int rowIndex, int columnIndex) {
//                switch(columnIndex) {
//                    case 2:
//                        return true;
//                    default :
//                        return false;
//                }
//            }
//
//        });
//        pnlProductList = new MaterialListPanel();
//        pnlProductList.addOnBuyProduct(this);
//        scrProductList.setViewportView(productListPanel);
    }
    
        private void refreshReciept() {
        tblShopMaterialReciept.revalidate();
        tblShopMaterialReciept.repaint();
//        lblTotal.setText("Total: " + reciept.getTotal());
    }/////////////////////////////(user with table)
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        ProductList = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblShopMaterialReciept = new javax.swing.JTable();
        btnDelete = new javax.swing.JButton();
        btnBuy = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(71, 49, 49));

        ProductList.setBackground(new java.awt.Color(255, 255, 255));

        tblShopMaterialReciept.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblShopMaterialReciept);

        btnDelete.setText("Delete");

        btnBuy.setText("Buy");
        btnBuy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuyActionPerformed(evt);
            }
        });

        btnClear.setText("Clear");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ProductList, javax.swing.GroupLayout.DEFAULT_SIZE, 560, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBuy, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 408, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(ProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 579, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(50, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane2)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBuy, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21))))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1010, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 624, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnBuyActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ShopMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ShopMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ShopMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ShopMaterial.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ShopMaterial().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane ProductList;
    private javax.swing.JButton btnBuy;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblShopMaterialReciept;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Material material, int qty) {
        materialReciept.addRecieptDetail(material, qty);
        refreshReciept();
    }
}
