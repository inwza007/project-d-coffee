/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author test1
 */
public class MaterialReciept {

    private int id; // mr_id
    private int userId; // user_id
    //private User user ;
//  private int supplierID ; //supplier_id
    //private Supplier ;
//  private int storeID ; // store_id
    //private Store;
    private int totalQty; // mr_cargo
    private float total ;
    private float price; // mr_cargo_price
//  private Date materialRecieptDate ; // mr_recived_date
    private Date createdDate; // mr_date
    private ArrayList<MaterialRecieptDetail> materialRecieptDetails = new ArrayList();
    

    public MaterialReciept(int id, int userId, int qty, int price) {
        this.id = id;
        this.userId = userId;
        this.totalQty = qty;
        this.price = price;
    }

    public MaterialReciept(int userId, int qty, int price) {
        this.id = -1;
        this.userId = userId;
        this.totalQty = qty;
        this.price = price;
    }

    public MaterialReciept(int qty, int price) {
        this.id = -1;
        this.userId = 0;
        this.totalQty = qty;
    }

    public MaterialReciept(int userId) {
        this.id = -1;
        this.userId = userId;
        this.totalQty = 0;
        this.price = 0;
    }

    public MaterialReciept() {
        this.id = -1;
        this.userId = 0;
        this.totalQty = 0;
        this.price = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getQty() {
        return totalQty;
    }

    public void setQty(int qty) {
        this.totalQty = qty;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public ArrayList<MaterialRecieptDetail> getMaterialRecieptDetails() {
        return materialRecieptDetails;
    }

    public void setMaterialRecieptDetails(ArrayList<MaterialRecieptDetail> materialRecieptDetails) {
        this.materialRecieptDetails = materialRecieptDetails;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    @Override
    public String toString() {
        return "MaterialReciept{" + "id=" + id + ", userId=" + userId + ", totalQty=" + totalQty + ", price=" + price + ", createdDate=" + createdDate + ", materialRecieptDetails=" + materialRecieptDetails + '}';
    }
    
    

    public void addRecieptDetail(MaterialRecieptDetail recieptDetail) {
        materialRecieptDetails.add(recieptDetail);
        calculateTotal();
    }

    public void addRecieptDetail(Material product, int qty) {
        MaterialRecieptDetail rd = new MaterialRecieptDetail();
        materialRecieptDetails.add(rd);
        calculateTotal();
    }

    public void delRecieptDetail(MaterialRecieptDetail recieptDetail) {
        materialRecieptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (MaterialRecieptDetail rd : materialRecieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.totalQty = totalQty;
        this.total = total;
    }

    public static MaterialReciept fromRS(ResultSet rs) {
        MaterialReciept obj = new MaterialReciept();
        try {
            obj.setId(rs.getInt("mr_id"));
//            obj.setCreatedDate(rs.getDate("mr_date"));
            obj.setUserId(rs.getInt("user_id"));
            obj.setQty(rs.getInt("mr_cargo"));
            obj.setPrice(rs.getFloat("mr_cargo_price"));
//            obj.setId(rs.getInt("mr_recived_date"));
            // Population
//            CustomerDao customerDao = new CustomerDao();
//            UserDao userDao = new UserDao();
//            Customer customer = customerDao.get(obj.getCustomerId());
//            User user = userDao.get(obj.getUserId());
//            obj.setCustomer(customer);
//            obj.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(MaterialReciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
