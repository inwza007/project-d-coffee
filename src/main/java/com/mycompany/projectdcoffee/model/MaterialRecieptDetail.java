/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author test1
 */
public class MaterialRecieptDetail {

    private int id;
    private int materialRecieptId;
    private int materialId;
    private int qty;
    private int totalQty;
    private String unit;
//    privaet Date exd ;
    private float totalPrice;

    public MaterialRecieptDetail(int id, int materialRecieptId, int materialId, int qty, int totalQty, String unit, float totalPrice) {
        this.id = id;
        this.materialRecieptId = materialRecieptId;
        this.materialId = materialId;
        this.qty = qty;
        this.totalQty = totalQty;
        this.unit = unit;
        this.totalPrice = totalPrice;
    }

    public MaterialRecieptDetail(int materialRecieptId, int materialId, int qty, int totalQty, String unit, float totalPrice) {
        this.id = -1;
        this.materialRecieptId = materialRecieptId;
        this.materialId = materialId;
        this.qty = qty;
        this.totalQty = totalQty;
        this.unit = unit;
        this.totalPrice = totalPrice;
    }
    
        public MaterialRecieptDetail(int materialRecieptId, int materialId, int qty, int totalQty, String unit) {
        this.id = -1;
        this.materialRecieptId = materialRecieptId;
        this.materialId = materialId;
        this.qty = qty;
        this.totalQty = totalQty;
        this.unit = unit;
        this.totalPrice = 0;
    }

    public MaterialRecieptDetail() {
        this.id = -1;
        this.materialRecieptId = 0;
        this.materialId = 0;
        this.qty = 0;
        this.totalQty = 0;
        this.unit = "Unit";
        this.totalPrice = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMaterialRecieptId() {
        return materialRecieptId;
    }

    public void setMaterialRecieptId(int materialRecieptId) {
        this.materialRecieptId = materialRecieptId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "MaterialRecieptDetail{" + "id=" + id + ", materialRecieptId=" + materialRecieptId + ", materialId=" + materialId + ", qty=" + qty + ", totalQty=" + totalQty + ", unit=" + unit + '}';
    }

    public static MaterialRecieptDetail fromRS(ResultSet rs) {
        MaterialRecieptDetail recieptDetail = new MaterialRecieptDetail();
        try {
            recieptDetail.setId(rs.getInt("mrd_id"));
            recieptDetail.setMaterialId(rs.getInt("material_id"));
            recieptDetail.setMaterialRecieptId(rs.getInt("mr_id"));
            recieptDetail.setQty(rs.getInt("mrd_qty"));
            recieptDetail.setTotalQty(rs.getInt("mrd_total"));
            recieptDetail.setUnit(rs.getString("mrd_unit"));
        } catch (SQLException ex) {
            Logger.getLogger(MaterialReciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return recieptDetail;
    }
}
