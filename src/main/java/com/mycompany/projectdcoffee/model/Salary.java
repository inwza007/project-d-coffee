/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author armme
 */
public class Salary {
    private int prId;
    private int userId;
    private String prApprove;
    private Date prDate;
    private String prType;
    private int prTotalTimeWork;
    private int prPricePerHour;
    private BigDecimal prTotalSalary;

    public Salary(int prId, int userId, String prApprove, Date prDate, String prType, int prTotalTimeWork, int prPricePerHour, BigDecimal prTotalSalary) {
        this.prId = prId;
        this.userId = userId;
        this.prApprove = prApprove;
        this.prDate = prDate;
        this.prType = prType;
        this.prTotalTimeWork = prTotalTimeWork;
        this.prPricePerHour = prPricePerHour;
        this.prTotalSalary = prTotalSalary;
    }
    
    public Salary(int userId, String prApprove, Date prDate, String prType, int prTotalTimeWork, int prPricePerHour, BigDecimal prTotalSalary) {
        this.prId = -1;
        this.userId = userId;
        this.prApprove = prApprove;
        this.prDate = prDate;
        this.prType = prType;
        this.prTotalTimeWork = prTotalTimeWork;
        this.prPricePerHour = prPricePerHour;
        this.prTotalSalary = prTotalSalary;
    }
    
    public Salary(int userId, String prApprove, String prType, int prTotalTimeWork, int prPricePerHour, BigDecimal prTotalSalary) {
        this.prId = -1;
        this.userId = userId;
        this.prApprove = prApprove;
        this.prDate = null;
        this.prType = prType;
        this.prTotalTimeWork = prTotalTimeWork;
        this.prPricePerHour = prPricePerHour;
        this.prTotalSalary = prTotalSalary;
    }
    
    public Salary() {
        this.prId = -1;
        this.userId = 0;
        this.prApprove = "";
        this.prDate = null;
        this.prTotalTimeWork = 0;
        this.prPricePerHour = 0;
        this.prTotalSalary = BigDecimal.ZERO;
    }

    public int getPrId() {
        return prId;
    }

    public void setPrId(int prId) {
        this.prId = prId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPrApprove() {
        return prApprove;
    }

    public void setPrApprove(String prApprove) {
        this.prApprove = prApprove;
    }

    public Date getPrDate() {
        return prDate;
    }

    public void setPrDate(Date prDate) {
        this.prDate = prDate;
    }

    public String getPrType() {
        return prType;
    }

    public void setPrType(String prType) {
        this.prType = prType;
    }

    public int getPrTotalTimeWork() {
        return prTotalTimeWork;
    }

    public void setPrTotalTimeWork(int prTotalTimeWork) {
        this.prTotalTimeWork = prTotalTimeWork;
    }

    public int getPrPricePerHour() {
        return prPricePerHour;
    }

    public void setPrPricePerHour(int prPricePerHour) {
        this.prPricePerHour = prPricePerHour;
    }

    public BigDecimal getPrTotalSalary() {
        return prTotalSalary;
    }

    public void setPrTotalSalary(BigDecimal prTotalSalary) {
        this.prTotalSalary = prTotalSalary;
    }

    @Override
    public String toString() {
        return "Salary{" + "prId=" + prId + ", userId=" + userId + ", prApprove=" + prApprove + ", prDate=" + prDate + ", prType=" + prType + ", prTotalTimeWork=" + prTotalTimeWork + ", prPricePerHour=" + prPricePerHour + ", prTotalSalary=" + prTotalSalary + '}';
    }
    
    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setPrId(rs.getInt("payroll_id"));
            salary.setUserId(rs.getInt("user_id"));
            salary.setPrApprove(rs.getString("payroll_approve"));
            salary.setPrDate(rs.getDate("payroll_date"));
            salary.setPrType(rs.getString("payroll_type"));
            salary.setPrTotalTimeWork(rs.getInt("payroll_total_time_work"));
            salary.setPrPricePerHour(rs.getInt("payroll_price_per_hour"));
            salary.setPrTotalSalary(rs.getBigDecimal("payroll_total_salary"));
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }
}
