/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.service;

import com.mycompany.databaseproject.dao.SalaryDao;
import com.mycompany.projectdcoffee.model.Salary;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author armme
 */
public class SalaryService {
    private SalaryDao salaryDao = new SalaryDao();
    public ArrayList<Salary> getSalarysOrderByName() {
        return (ArrayList<Salary>) salaryDao.getAll(" payroll_id asc");
    }
    
    public Salary getByPrId(int prId ) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.get(prId);
    }
    
    public List<Salary> getSalarys(){
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getAll(" payroll_id asc");
    }

    public Salary addNew(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.save(editedSalary);
    }

    public Salary update(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.update(editedSalary);
    }

    public int delete(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.delete(editedSalary);
    }
}
