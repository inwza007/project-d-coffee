    /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.service;

import com.mycompany.databaseproject.dao.CheckTimeDao;
import com.mycompany.projectdcoffee.model.CheckTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phattharaphon
 */
public class CheckTimeService {

    private final CheckTimeDao checkTimeDao = new CheckTimeDao();
//    public List<CheckTime> getCheckTimes() {

    public ArrayList<CheckTime> getCheckTimeOrderByName() {
        return (ArrayList<CheckTime>) checkTimeDao.getAll(" wh_id asc");
    }

//    public CheckTime update(CheckTime editedCheckTime) {
//        CheckTimeDao checkTimeDao = new CheckTimeDao();
//        return checkTimeDao.update(editedCheckTime);
//    }
}
