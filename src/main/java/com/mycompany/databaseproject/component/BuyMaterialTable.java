/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.component;

import com.mycompany.projectdcoffee.model.Material;

public interface BuyMaterialTable {
    public void buy (Material material, int qty);
}
