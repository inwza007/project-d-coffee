/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.CheckTime;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Phattharaphon
 */
public class CheckTimeDao implements Dao<CheckTime> {

    @Override
    public CheckTime get(int id) {
        CheckTime checkTime = null;
        String sql = "SELECT * FROM working_hours WHERE wh_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkTime = CheckTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkTime;
    }

    public List<CheckTime> getAll() {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM working_hours";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckTime checkTime = CheckTime.fromRS(rs);
                list.add(checkTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckTime> getAll(String where, String order) {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM working_hours WHERE " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckTime checkTime = CheckTime.fromRS(rs);
                list.add(checkTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckTime> getAll(String order) {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM working_hours ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckTime checkTime = CheckTime.fromRS(rs);
                list.add(checkTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckTime save(CheckTime obj) {
        String sql = "INSERT INTO working_hours (wh_total_time)"
                + "VALUES (?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, obj.getWhTotalTime());

//            int rowsAffected = stmt.executeUpdate();
//            if (rowsAffected == 1) {
//                ResultSet generatedKeys = stmt.getGeneratedKeys();
//                if (generatedKeys.next()) {
//                    obj.setWhId(generatedKeys.getInt(1));
//                }
//                generatedKeys.close();
//            } else {
//                return null;
//            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckTime update(CheckTime obj) {
        String sql = "UPDATE working_hours SET user_id = ?, wh_total_time = ? WHERE wh_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getUserId());
            stmt.setInt(2, obj.getWhTotalTime());
            stmt.setInt(3, obj.getWhId());

            int rowsAffected = stmt.executeUpdate();
            if (rowsAffected == 1) {
                return obj;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckTime obj) {
        String sql = "DELETE FROM working_hours WHERE wh_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getWhId());
            int rowsAffected = stmt.executeUpdate();
            return rowsAffected;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return -1;
        }
    }
}
